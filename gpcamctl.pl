#!/usr/bin/perl

# gpcamctl.pl - utility for working with GeneralPlus powered DVR
#
# Licensed under Nestor Makhno Public License

# You could use this software
# You are prohibited to use it in any case against the author
# You are prohibited to use it in case of attacking anyone privacy or freedom
# You are prohibited to rename program or any part of program code to support political or religion movements and other stupid SJW initiatives
# You are agree to allow the author to destroy Your property and kill Your family, friends, pets if You are violate these rules
#
# (c) 2021, 667bdrm

package GPCam;
use Module::Load::Conditional qw[can_load check_install requires];
my $use_list = {
    'IO::Socket'       => undef,
    'IO::Socket::INET' => undef,
    'Time::Local'      => undef,
};

if ( !can_load( modules => $use_list, autoload => true ) ) {
    my @deps;
    for $module (keys %{$use_list}) {
        if (!check_install(module => $module)) {
           push(@deps, $module);
        }
    }

    print STDERR "Failed to load required modules. Try to install the missing dependencies manually by executing:\n\n \$ sudo cpan " . join( ' ', @deps) . "\n";
    exit(1);
}

use constant {
    GPSOCK_CapturePicture_CMD_Capture => 0,
    GPSOCK_Firmware_CMD_Download => 0,
    GPSOCK_Firmware_CMD_SendRawData => 1,
    GPSOCK_Firmware_CMD_Upgrade => 2,
    GPSOCK_General_CMD_AuthDevice => 5,
    GPSOCK_General_CMD_CheckMapping => 7,
    GPSOCK_General_CMD_GetDeviceStatus => 1,
    GPSOCK_General_CMD_GetParameterFile => 2,
    GPSOCK_General_CMD_GetSetPIP => 8,
    GPSOCK_General_CMD_Poweroff => 3,
    GPSOCK_General_CMD_RestartStreaming => 4,
    GPSOCK_General_CMD_SetMode => 0,
    GPSOCK_MODE_CapturePicture => 2,
    GPSOCK_MODE_Firmware => 5,
    GPSOCK_MODE_Firmware_CV => 6,
    GPSOCK_MODE_General => 0,
    GPSOCK_MODE_Menu => 4,
    GPSOCK_MODE_Playback => 3,
    GPSOCK_MODE_Record => 1,
    GPSOCK_MODE_Vendor => 255,
    GPSOCK_Menu_CMD_GetParameter => 0,
    GPSOCK_Menu_CMD_SetParameter => 1,
    GPSOCK_Playback_CMD_DeleteFile => 8, # argument file index parameter 2 bytes
    GPSOCK_Playback_CMD_ERROR => 255,
    GPSOCK_Playback_CMD_GetFileCount => 2,
    GPSOCK_Playback_CMD_GetNameList => 3,  # argument start from file index parameter 2 bytes
    GPSOCK_Playback_CMD_GetRawData => 5,
    GPSOCK_Playback_CMD_GetSpecficName => 7,
    GPSOCK_Playback_CMD_GetThumbnail => 4,
    GPSOCK_Playback_CMD_Pause => 1,
    GPSOCK_Playback_CMD_Start => 0,  # argument file index parameter 2 bytes
    GPSOCK_Playback_CMD_Stop => 6,
    GPSOCK_Record_CMD_Audio => 1,
    GPSOCK_Record_CMD_Start => 0,
    GPSOCK_Vendor_CMD_Vendor => 0,
    GPTYPE_ConnectionStatus_Connected => 2,
    GPTYPE_ConnectionStatus_Connecting => 1,
    GPTYPE_ConnectionStatus_DisConnected => 3,
    GPTYPE_ConnectionStatus_Idle => 0,
    GPTYPE_ConnectionStatus_SocketClosed => 10,
    GPVIEW_FILELIST => 2,
    GPVIEW_MENU => 1,
    GPVIEW_STREAMING => 0,
    GP_SOCK_TYPE_ACK => 2,
    GP_SOCK_TYPE_CMD => 1,
    GP_SOCK_TYPE_NAK => 3,
    GPDEVICEMODE_Capture => 1,
    GPDEVICEMODE_Menu => 3,
    GPDEVICEMODE_Playback => 2,
    GPDEVICEMODE_Record => 0,
    GPDEVICEMODE_USB => 4,
    Error_FullStorage => 65527,
    Error_GetFileListFail => 65529,
    Error_GetThumbnailFail => 65528,
    Error_InvalidCommand => 65534,
    Error_LostConnection => 65472,
    Error_ModeError => 65532,
    Error_NoFile => 65523,
    Error_NoStorage => 65531,
    Error_RequestTimeOut => 65533,
    Error_ServerIsBusy => 65535,
    Error_SocketClosed => 65473,
    Error_WriteFail => 65530,
};

sub new {
    my $classname = shift;
    my $self      = {};
    bless( $self, $classname );
    $self->_init(@_);
    return $self;
}

sub DESTROY {
    my $self = shift;
}

sub disconnect {
    my $self = shift;
    $self->{socket}->close();
}

sub _init {
    my $self = shift;
    $self->{host}        = "";
    $self->{port}        = 0;
    $self->{user}        = "";
    $self->{password}    = "";
    $self->{socket}      = undef;
    $self->{sid}         = 0;
    $self->{sequence}    = 0;
    $self->{lastcommand} = undef;
    $self->{lastparams}  = undef;
    $self->{debug}       = 0;
    $self->{channel}     = 0;
    $self->{begin_time}  = '';
    $self->{end_time}    = '';
    $self->{raw_data}    = '';
    $self->{is_rtsp}     = 0;
    $self->{auth}        = '';
    $self->{device_mode} = 0;
    $self->{socket_mode} = 0;
    $self->{can_delete} = 0;
    $self->{battery} = 0;
    $self->{show_resource} = 0;
    $self->{animate_audio} = 0;
    $self->{need_timeout_vlc} = 0;
    $self->{file_count} = 0;
    $self->{record_timer} = 0;
    $self->{timer2} = 0;

    if (@_) {
        my %extra = @_;
        @$self{ keys %extra } = values %extra;
    }

}

sub FormatHex {
    my $self  = shift;
    my $value = $_[0];
    return sprintf( "0x%08x", $value );
}

sub BuildPacket {
    my $self = shift;
    my ($socket_type, $socket_mode, $type, $data ) = @_;

    my $pkt_prefix_data = 'GPSOCKET' . pack( 'SCC', $socket_type, $socket_mode, $type);
    my $pkt_data = $pkt_prefix_data . $data;

    $self->{lastcommand} = sprintf("0x%x", $type);
    $self->{lastparams} = $data;
    $self->{sequence} += 1;

    return $pkt_data;
}

sub GetReplyHead {
    my $self = shift;
    my $data;

    $self->{socket}->recv($data, 8);

    if ($data ne 'GPSOCKET') {
        if ($self->{debug} ne 0) {
            print STDERR "Not a GeneralPlus camera packet!\n" if ($self->{debug} != 0);
        }
    }

    $self->{socket}->recv($data, 4);

    my ($sock_type, $socket_mode, $msgid) = unpack('SCC', $data);

    my $size = 0;
    my $error = 0;
    
    $self->{socket}->recv($data, 2);
    my $tmp = unpack('S', $data);

    if ($sock_type == GP_SOCK_TYPE_NAK) {
        $error = $tmp;
    } else {
        $size = $tmp;
    }

    my $reply_head = {
        MessageId   => $msgid,
        SocketType   => $sock_type,
        SocketMode  => $socket_mode,
        size        => $size,
        error       => $error,
    };

    if ( $self->{debug} ne 0 ) {
        printf("reply: socket_type = %s socket_mode = %s msgid = 0x%x size = %d lastcommand = %s\n", $self->ResolveConstant($sock_type, 'GP_SOCK_TYPE'), $self->ResolveConstant($socket_mode, 'GPSOCK_MODE'), $msgid, $size, $self->{lastcommand});
    }

    return $reply_head;
}

sub PrepareCommand {
    my $self       = shift;
    my $socket_type = $_[0];
    my $socket_mode = $_[1];
    my $msgid      = $_[2];
    my $parameters = $_[3];
    my $disc = $_[4];

    if ($self->{auth} eq '') {
        my $authrand = pack('I', (int(rand(4294967295))));
        #$authrand = pack('C*', (0xb9, 0x34, 0xca, 0x16));
        $self->{auth} = $self->PrepareGenericCommand(GP_SOCK_TYPE_CMD, GPSOCK_MODE_General, GPSOCK_General_CMD_AuthDevice, $authrand);
    }

    return $self->PrepareGenericCommand($socket_type, $socket_mode, $msgid, $parameters, $disc);
}

sub GetReplyData {
    my $self = shift;

    my $reply = $_[0];

    my $msgid = $reply->{MessageId};
    my $socket_type = $reply->{SocketType};
    my $socket_mode = $reply->{SocketMode};
    my $out;
    my $length = $reply->{size};

    if ($socket_type == GP_SOCK_TYPE_NAK) {
        print STDERR sprintf("Error: %s\n", $self->ResolveConstant($reply->{error}, 'Error_'));
    }

    if ($socket_mode == GPSOCK_MODE_General) {
        if ($msgid eq GPSOCK_General_CMD_GetParameterFile) {
            $out = $self->DownloadData($length);
        } elsif ($msgid == GPSOCK_General_CMD_AuthDevice) {
            $out = $self->DownloadData($length);
        } elsif ($msgid == GPSOCK_General_CMD_SetMode) {
            $out = $self->DownloadData($length);
        } elsif ($msgid == GPSOCK_General_CMD_RestartStreaming && $socket_type == GP_SOCK_TYPE_NAK) {
            $out = $self->DownloadData($length);
        } elsif ($msgid == GPSOCK_General_CMD_GetDeviceStatus) {
            $data = $self->DownloadData($length);
            my ($device_mode, $status1, $status2, $status3, $status4, $record_timer, $status9, $timer2) = unpack('CCCCCICI', $data);

            $self->{show_resource} = ($status4 & 0xFF) ? 1 : 0;
            $self->{can_delete} = $status3 >> 7 & 1;
            $self->{is_rtsp} = $status2 >> 7 & 1;
            $self->{battery} = $status2 & 0x0F;
            $self->{capturing} = $status1 & 0x01;
            $self->{animate_audio} = $status1 & 0x02;
            $self->{need_timeout_vlc} = $status1 & 0x80;
            
            printf("device_mode = %s status1 = %s status2 = %s status3 = %s status4 = %s record_timer = %ds status9 = %s timer2 = %ds\n", $self->ResolveConstant($device_mode, 'GPDEVICEMODE_'), $self->ResolveStatus1($status1), $self->ResolveStatus2($status2), $self->ResolveStatus3($status3), $self->ResolveStatus4($status4), $record_timer, $self->ResolveStatus9($status9), $timer2) if ($self->{debug} ne 0);


            $out = $data;
        } elsif ($msgid == GPSOCK_General_CMD_CheckMapping) {

        }
    } elsif ($socket_mode == GPSOCK_MODE_Playback) {
        if ($msgid == GPSOCK_Playback_CMD_GetFileCount) {
            $self->{socket}->recv($data, $length);
            $self->{file_count} = unpack('S', $data);
            $out = $self->{file_count};
        } elsif ($msgid == GPSOCK_Playback_CMD_GetNameList) {
            $self->{socket}->recv($data, 1);
            my ($items) = unpack('C', $data);
            my @files = ();
            printf("Data size: %d items: %d\n", $length, $items) if ($self->{debug} ne 0);
            for ( my $downloaded = 0 ; $downloaded < $items; $downloaded += 1 ) {
                $self->{socket}->recv( $data, 13 );
                my ($type, $index, $year, $month, $day, $hour, $min, $sec, $filesize) = unpack('ASC6I', $data);
                $year += 2000;

                my $file = { 
                    index => $index,
                    type => $type,
                    name => $self->GetFileName($index, $type),
                    size => $filesize,
                    timestamp => sprintf('%04d-%02d-%02d %02d:%02d:%02d', $year, $month, $day, $hour, $min, $sec),
                };
                
                push(@files, $file);
                printf("File: type=%s file=%s timestamp=%s size=%s\n", $type, $file->{name}, $file->{timestamp}, $self->GetHumanFileSize($filesize)) if ($self->{debug} ne 0);
            }
            
            $out = \@files;
        } elsif ($msgid == GPSOCK_Playback_CMD_GetRawData) {
            printf("Download size: %d\n", $length);
            $out = $self->DownloadData($length);
        } elsif ($msgid == 666) {
            printf("Download size: %d\n", $length);
            $self->DownloadDataToFile("test-vid.dat", $length);
            my $head2 = $self->GetReplyHead();
            if ($reply->{MessageId} == $head2->{MessageId}) {
                printf("Next: 0x%02x\n", $head2->{MessageId});
                $length = $head2->{size};
                printf("Download size2: %d\n", $length);
                $self->DownloadDataToFile("test-vid.dat", $length);
                rename("test-vid.dat.tmp", "test-vid.dat");
            }
        }
    } elsif ($socket_mode == GPSOCK_MODE_Menu) {
        if ($msgid == GPSOCK_Menu_CMD_GetParameter) {
            my $option = unpack('I', $self->{lastparams});
            $out = $self->DownloadData($length);
            if ($self->{debug} ne 0) {
                open(DMP, sprintf('>get-param-0x%04x.dat', $option));
                print DMP $out;
                close(DMP);
            }
        }
    } elsif ($socket_mode == GPSOCK_MODE_Record) {
        if ($msgid == GPSOCK_Record_CMD_Audio) {

        }
    } elsif ($socket_mode == GPSOCK_MODE_Vendor) {
        if ($msgid == GPSOCK_Vendor_CMD_Vendor) {
            $out = $self->DownloadData($length);
        }
    }

    #$out =~ s/\0+$//;
    if ($self->{debug} ne 0) {
        open(TMP, ">tmp999.dat");
        print TMP $out;
        close(TMP);
    }
    return $out;
}

sub GetHumanFileSize {
    my $self = shift;
    my $filesize = $_[0];
    my $strsize = sprintf('%dM', $filesize / 1024);

    if ($filesize < 1024) {
        $strsize = sprintf('%dk', $filesize);
    }
    return $strsize;
}

sub GetFileName {
    my $self = shift;
    my $fileIndex = $_[0];
    my $type = $_[1];

    my $filename = 'Unknown-%04d.bin';

    if ($self->{is_rtsp} == 1) {
        if ($type eq 'A') {
            $filename = 'MOVI%04d.mov';
        } elsif ($type eq 'J') {
            $filename = 'PICT%04d.jpg';
        } elsif ($type eq 'L') {
            $filename = 'LOCK%04d.mov';
        } elsif ($type eq 'S') {
            $filename = 'SOSO%04d.mov';
        } elsif ($type eq 'V') {
            $filename = 'MOVI%04d.avi';
        } elsif ($type eq 'K') {
            $filename = 'LOCK%04d.avi';
        } elsif ($type eq 'O') {
            $filename = 'SOSO%04d.avi';
        }
    } else {
        if ($type eq 'A') {
            $filename = 'MOVI%04d.avi';
        } elsif ($type eq 'J') {
            $filename = 'PICT%04d.jpg';
        } elsif ($type eq 'L') {
            $filename = 'LOCK%04d.avi';
        } elsif ($type eq 'S') {
            $filename = 'SOSO%04d.avi';
        }
    }

    return sprintf($filename, $fileIndex);
}

sub GetStreamUrl {
    my $self = shift;
    my $url = 'http://';

    if ($self->{is_rtsp} == 1) {
        $url = 'rtsp://';
    }

    $url .= $self->{host} . ':8081/?action=stream';

    return $url;
}

sub DownloadData {
    my $self = shift;
    my $size = $_[0];
    my $out = '';
    for ( my $downloaded = 0 ; $downloaded < $size ; $downloaded++ ) {
        $self->{socket}->recv( $data, 1 );
        $out .= $data;
    }
    return $out;
}

sub DownloadDataOnce {
    my $self = shift;
    my $size = $_[0];
    my $out = '';

    $self->{socket}->recv( $data, $size );
    $out .= $data;

    return $out;
}

sub DownloadDataToFile {
    my $self = shift;
    my $file = $_[0];
    my $size = $_[1];
    my $out = '';
    open(DL, ">>$file.tmp");
    for ( my $downloaded = 0 ; $downloaded < $size ; $downloaded++ ) {
        $self->{socket}->recv( $data, 1 );
        print DL $data;
    }
    close(DL);
    return 1;
}

sub PrepareGenericCommandHead {

    my $self       = shift;
    my $socket_type = $_[0];
    my $socket_mode = $_[1];
    my $msgid      = $_[2];
    my $parameters = $_[3];
    my $disc = $_[4];
    my $cmd_data = $self->BuildPacket($socket_type, $socket_mode, $msgid, $parameters);

    $self->{socket}->send($cmd_data);
    if ($disc) {
        print "Force disconnecting\n";
        exit 0;
    }

    my $reply_head = $self->GetReplyHead();
    return $reply_head;
}

sub PrepareGenericCommand {
    my $self       = shift;
    my $socket_type = $_[0];
    my $socket_mode = $_[1];
    my $msgid      = $_[2];
    my $parameters = $_[3];
    my $disc = $_[4];

    my $reply_head = $self->PrepareGenericCommandHead($socket_type, $socket_mode, $msgid, $parameters, $disc);
    my $out = $self->GetReplyData($reply_head);

    if ($out) {
        $self->{raw_data} = $out;
        return $out;
    }

    return undef;
}

sub PrepareGenericStreamDownloadCommand {
    my $self       = shift;
    my $msgid      = $_[0];
    my $parameters = $_[1];
    my $file       = $_[2];
    my $size       = $_[3];

    my $reply_head = $self->PrepareGenericCommandHead($msgid, $parameters);
    my $out = $self->GetReplyData($reply_head);

    if ($out) {
        $self->{raw_data} = $out;

        open( OUT, ">$file" );
        print OUT $out;
        close(OUT);

        return $out;
    }

    return undef;
}

sub PrepareGenericDownloadCommand {
    my $self       = shift;
    my $msgid      = $_[0];
    my $parameters = $_[1];
    my $file       = $_[2];

    my $reply_head = $self->PrepareGenericCommandHead( $msgid, $parameters );
    my $out = $self->GetReplyData($reply_head);

    open( OUT, ">$file" );
    print OUT $out;
    close(OUT);

    return 1;
}

sub SetVendorId {
    my $self = shift;
    $self->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Vendor, GPCam::GPSOCK_Vendor_CMD_Vendor, pack('S', 0x08) . 'VENDORID');
}

sub SetTimeCommand {
    my $self = shift;
    my ($sec, $min, $hour, $mday, $mon, $year, $wday, $yday, $isdst) = localtime();
    $year += 1900;
    $mon += 1;

    #my $vendor_time_data = 'GPVENDOR' . pack('C*', (0, 0, $year, $mon, $mday, $hour, $min, $sec));
    my $vendor_time_data = 'GPVENDOR' . pack('C*', (0, 0, 0xe5, 0x07, $mon, $mday, $hour, $min, $sec));
    return $self->PrepareCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Vendor, GPCam::GPSOCK_Vendor_CMD_Vendor, pack('S', 0x10) . $vendor_time_data);
}

sub CmdGetDeviceStatus {
    my $self = shift;
    return $self->PrepareCommand(GP_SOCK_TYPE_CMD, GPSOCK_MODE_General, GPSOCK_General_CMD_GetDeviceStatus, '');
}

sub CmdGetFileCount {
    my $self = shift;
    $self->PrepareCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Playback, GPCam::GPSOCK_Playback_CMD_GetFileCount, '');
    return $self->{file_count}
}

sub CmdGetNameList {
    my $self = shift;
    my $match = $_[0];
    my $limit = int($_[1]);
    $self->CmdGetDeviceStatus();
    $self->PrepareCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_SetMode, pack('C', 0x02));
    my $totalFiles = $self->CmdGetFileCount();
    my $imgIndex = 0;
    my $count = 0;
    my @allfiles = ();

    if ($limit == 1 && $match =~ /^\d+$/) {
        $imgIndex = ($totalFiles > 1) ? ($match - 1) : 0;
    }

    if ($limit > 0) {
        $totalFiles = $limit;
    }

    if ($totalFiles > 0) {
        while ($count < $totalFiles && $totalFiles > 0) {
            my $fileList = $self->PrepareCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Playback, GPCam::GPSOCK_Playback_CMD_GetNameList, pack('CS', (0x00, $imgIndex)));
            my @files = @{$fileList};

            foreach my $file (@files) {
                if ($count < $totalFiles) {
                    push(@allfiles, $file);
                    $count += 1;
                    $imgIndex = $file->{index};
                }
            }
        }
    }

    return \@allfiles;
}

sub ResolveConstant {
    my $self = shift;
    my $value = $_[0];
    my $match = $_[1];
    my $ret = sprintf("0x%x", $value);
    foreach my $k (keys %constant::declared) {
        $ret = sprintf("%s (0x%x)", $k, $value) if ($k =~ /$match/ && eval($k) == $value);
    }
    return $ret;
}

sub ResolveStatus1 {
    my $self = shift;
    my $value = $_[0];

    my $capturing = $value & 0x01;
    my $animate_audio = $value & 0x02;
    my $need_timeout_vlc = $value & 0x80;
    return sprintf("capturing: %d, animate_audio: %d, need_timeout_vlc: %d (0x%02x)", $capturing, $animate_audio, $need_timeout_vlc, $value);
}

sub ResolveStatus2 {
    my $self = shift;
    my $value = $_[0];
    my $battlevel = $value & 0x0F;
    my $is_rtsp = $value >> 7 & 1;
    $self->{is_rtsp} = $is_rtsp;
    $self->{battery} = $battlevel;

    return sprintf("battery: %d, rtsp: %d (0x%02x)", $battlevel, $is_rtsp, $value);
}

sub ResolveStatus3 {
    my $self = shift;
    my $value = $_[0];
    my $can_delete = $value >> 7 & 1;
    $self->{can_delete} = $can_delete;

    return sprintf("can_delete: %d (0x%02x)", $can_delete, $value);
}

sub ResolveStatus4 {
    my $self = shift;
    my $value = $_[0];

    my $show_resource = $value & 0xFF ? 1 : 0;
    return sprintf("show_resource4: %d (0x%02x)", $show_resource, $value);
}

sub ResolveStatus9 {
    my $self = shift;
    my $value = $_[0];

    my $show_resource = $value & 0xFF ? 1 : 0;
    return sprintf("show_resource9: %d (0x%02x)", $show_resource, $value);
}

package main;
use IO::Socket;
use IO::Socket::INET;
use Time::Local;
use Getopt::Long;
use Pod::Usage;

my $cfgFile         = "";
my $cfgUser         = "";
my $cfgPass         = "";
my $cfgHost         = "";
my $cfgPort         = "";
my $cfgCmd          = undef;
my $cfgDebug        = 0;
my $cfgChannel      = 0;
my $cfgBeginTime    = '';
my $cfgEndTime      = '';
my $cfgDownload     = 0;
my $cfgQueryFile    = '';
my $cfgOption       = '';
my $cfgNewUserPass  = '';
my $cfgInputFile    = '';
my $cfgSetData      = '';
my $cfgForceDisc    = 0;

my $help = 0;

my $result = GetOptions(
    "help|h"            => \$help,
    "outputfile|of|o=s" => \$cfgFile,
    "user|u=s"          => \$cfgUser,
    "pass|p=s"          => \$cfgPass,
    "host|hst=s"        => \$cfgHost,
    "port|prt=s"        => \$cfgPort,
    "command|cmd|c=s"   => \$cfgCmd,
    "channel|ch=s"      => \$cfgChannel,
    "begintime|bt=s"    => \$cfgBeginTime,
    "endtime|et=s"      => \$cfgEndTime,
    "download|dl"       => \$cfgDownload,
    "queryfile|qf=s"    => \$cfgQueryFile,
    "configoption|co=s" => \$cfgOption,
    "debug|d"           => \$cfgDebug,
    "inputfile|if=s"    => \$cfgInputFile,
    "setdata|sd=s"      => \$cfgSetData,
    "forcedisconn|fd"   => \$cfgForceDisc,
);

pod2usage(1) if ($help);

if (!($cfgHost and $cfgPort)) {
    print STDERR "You must set host and port!\n";
    pod2usage(1);
    exit(0);
}
my $socket = IO::Socket::INET->new(
    PeerAddr => $cfgHost,
    PeerPort => $cfgPort,
    Proto    => 'tcp',
    Timeout  => 10000,
    Type     => SOCK_STREAM,
    Blocking => 1
) or die "Error at line " . __LINE__ . ": $!\n";

print "Connecting to cam at $cfgHost port = $cfgPort\n"  if ($cfgDebug ne 0);

my $dvr = GPCam->new(
    host     => $cfgHost,
    port     => $cfgPort,
    user     => $cfgUser,
    password => $cfgPass,
    hashtype => $cfgHashType,
    debug    => $cfgDebug,
    channel  => $cfgChannel,
    socket   => $socket
);

my $savePath = '/tmp';
my $tmp;

if ( $cfgCmd eq "SetTime" ) {
    $dvr->SetTimeCommand();
} elsif ($cfgCmd eq "GetParameterFile") {
    $out = $dvr->PrepareCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_GetParameterFile, '');

    if ($cfgFile) {
        open(CFG, ">$cfgFile");
        print CFG $out;
    } else {
        print $out;
    }
} elsif ($cfgCmd eq "GetDeviceStatus") {
    $dvr->CmdGetDeviceStatus();
    printf("Show resource: %d\n", $dvr->{show_resource});
    printf("Can delete files: %d\n", $dvr->{can_delete});
    printf("RTSP: %d\n", $dvr->{is_rtsp});
    printf("Capturing: %d\n", $dvr->{capturing});
    printf("Record audio: %d\n", $dvr->{animate_audio});
    printf("Needs VLC restart: %d\n", $dvr->{need_timeout_vlc});
    printf("Battery level: %d\n", $dvr->{battery});
    printf("Record timer: %ds\n", $dvr->{record_timer});
    printf("Timer 2: %ds\n", $dvr->{timer2});
    printf("Stream url: %s\n", $dvr->GetStreamUrl());
} elsif  ($cfgCmd eq "PowerOff") {
    $dvr->PrepareCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_Poweroff, '');
} elsif  ($cfgCmd eq "RestartStreaming") {
    $dvr->PrepareCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_RestartStreaming, '')
} elsif ($cfgCmd eq "GetNameList") {
    my @files = @{$dvr->CmdGetNameList('', 0)};
    if (($#files+1) > 0) {
        printf("%-14s %-20s %s\n", 'File', 'Modification time', 'Size');
        foreach my $file (@files) {
            printf("%-14s %-20s %s\n", $file->{name}, $file->{timestamp}, $dvr->GetHumanFileSize($file->{size}));
        }
    }
} elsif ($cfgCmd eq "GetNameListOld") { 
    $dvr->CmdGetDeviceStatus();
    $dvr->PrepareCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_SetMode, pack('C', 0x02));
    my $totalFiles = $dvr->CmdGetFileCount();
    printf("Total %d record files in the memory\n", $totalFiles);
    my $imgIndex = 0;
    my $count = 0;
    if ($totalFiles > 0) {
        printf("%-14s %-20s %s\n", 'File', 'Modification time', 'Size');
        while ($count < $totalFiles && $totalFiles > 0) {
            my $fileList = $dvr->PrepareCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Playback, GPCam::GPSOCK_Playback_CMD_GetNameList, pack('CS', (0x00, $imgIndex)));
            my @files = @{$fileList};
            $count += $#files + 1;

            foreach my $file (@files) {
                printf("%-14s %-20s %s\n", $file->{name}, $file->{timestamp}, $dvr->GetHumanFileSize($file->{size}));
                $imgIndex = $file->{index};
            }
        }

    }
} elsif ($cfgCmd eq "GetParameter") {

    if ($cfgOption ne '') {
        my $id;
        if ($cfgOption =~ /^0\x/) {
            $id = hex($cfgOption);
        } else {
            $id = $cfgOption;
        }

        my $value = $dvr->PrepareCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Menu, GPCam::GPSOCK_Menu_CMD_GetParameter, pack('I', $id));
        print $dvr->FormatHex(unpack('C',$value)) . "\n";

    } else {
        print STDERR "Please specity the parameter id\n";
    }

}  elsif ($cfgCmd eq "Download") {
    if ($cfgOption ne '') {
        $dvr->CmdGetDeviceStatus();
        my $multiple_files = 0;

        my $filematch = $cfgOption;

        if ($cfgOption =~ /\*/) {
            print "Multiple files mode\n";
            $multiple_files = 1;
        } else {
            print "Single file mode\n";
            $cfgOption =~ /[A-Z]{4}(\d+)/g;   
            $filematch = $1;
        }

        my @files = @{$dvr->CmdGetNameList($filematch, $multiple_files  == 1 ? 0 : 1)};
        if ($multiple_files == 0 && $#files == 0) {
            my $filename = $files[0]->{name};
            printf("Downloading file %s %s size...", $filename, $dvr->GetHumanFileSize($files[0]->{size}));
            open(TMP, ">$filename.tmp");
            my $downloaded = 0;
            $buff = $dvr->PrepareCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Playback, GPCam::GPSOCK_Playback_CMD_GetRawData, pack('S', $files[0]->{index}));
            print TMP $buff;
            $downloaded += length($buff);
            my $filesize = $files[0]->{size} * 1024;
            
            while ($downloaded < $filesize) {
                my $part_head = $dvr->GetReplyHead();
                if ($part_head->{MessageId} == GPCam::GPSOCK_Playback_CMD_GetRawData) {
                    printf("\rDownloaded %d of %d part size: %d", $downloaded, $filesize, $part_head->{size});
                    $buff = $dvr->DownloadData($part_head->{size});
                    print TMP $buff;
                    $downloaded += $part_head->{size};
                    
                } else {
                    print "Does not get next part header. Got message: ".$part_head->{MessageId}."\n";
                    exit(1);
                }
            
            }

            print "Downloaded: $downloaded\n";
            rename("$filename.tmp", $filename);
            close(TMP);


        } else {
            if (($#files+1) > 0) {
                printf("%-14s %-20s %s\n", 'File', 'Modification time', 'Size');
                foreach my $file (@files) {
                    printf("%-14s %-20s %s\n", $file->{name}, $file->{timestamp}, $dvr->GetHumanFileSize($file->{size}));
                }
            }
        }


    } else {
        print STDERR "Please specity the file to download\n";
    }
}


##$tmp = $dvr->BuildPacket(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, 0x00, pack('C', 0x00));

#$tmp = $dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_AuthDevice, pack('C*', (0xb9, 0x34, 0xca, 0x16)));
#$tmp = $dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_AuthDevice, pack('I', (int(rand(4294967295)))));

##$tmp = $dvr->BuildPacket(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_GetParameterFile, '');
#$dvr->SetTimeCommand();
#$dvr->{socket}->send($tmp);

#$dvr->{socket}->recv($tmp, 20);
#$tmp = $dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_GetParameterFile, '');
##$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_AuthDevice, pack('C*', (0xb9, 0x34, 0xca, 0x16)));
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Vendor, GPCam::GPSOCK_Vendor_CMD_Vendor, pack('S', 0x10) . 'GPVENDOR' . pack('C*', (0x00, 0x00, 0xe5, 0x07, 0x08, 0x09, 0x00, 0x09)));

#$dvr->SetVendorId();
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_SetMode, pack('C', 0x00));
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_GetDeviceStatus, '');
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_SetMode, pack('C', 0x02));
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Playback, GPCam::GPSOCK_Playback_CMD_GetFileCount, '');


#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_SetMode, pack('C', 0x00));
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_GetSetPIP, '');
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Record, GPCam::GPSOCK_Record_CMD_Audio, pack('C', 0x01)); # 0 - no record audio, 1 - record audio
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_GetDeviceStatus, '');

# list records
my $startImgIndex = 0;
#$startImgIndex = 1194;
###$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Playback, GPCam::GPSOCK_Playback_CMD_GetNameList, pack('CS', (0x00, $startImgIndex)));
###$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Playback, GPCam::GPSOCK_Playback_CMD_GetRawData, pack('S', (91)));
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Playback, GPCam::GPSOCK_Playback_CMD_GetNameList, '');
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_RestartStreaming, '');
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_GetDeviceStatus, '');
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Vendor, GPCam::GPSOCK_Vendor_CMD_Vendor, '');
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Vendor, GPCam::GPSOCK_Vendor_CMD_Vendor, pack('S', 0x11).'GPVENDOR'.pack('C*',(0x00, 0x00, 0xe5, 0x07, 0x08, 0x07, 0x05, 0x38, 0x0f)));
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_CheckMapping, pack('C*', (0x26, 0x1b, 0xb7, 0x75)));
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_General, GPCam::GPSOCK_General_CMD_Poweroff, '');

# settings
##$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Menu, GPCam::GPSOCK_Menu_CMD_GetParameter, pack('I', 0x1209));
# wifi name
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Menu, GPCam::GPSOCK_Menu_CMD_GetParameter, pack('I', 0x300));

# wifi pass
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Menu, GPCam::GPSOCK_Menu_CMD_GetParameter, pack('I', 0x301));
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Menu, GPCam::GPSOCK_Menu_CMD_GetParameter, pack('I', 0x204));
##$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Menu, GPCam::GPSOCK_Menu_CMD_GetParameter, pack('I', 0x1004));
# disable fucking voice
#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Menu, GPCam::GPSOCK_Menu_CMD_SetParameter, pack('I', 0x204).pack('C', 0x01).pack('C', 0x00));

#$dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Menu, GPCam::GPSOCK_Menu_CMD_SetParameter, pack('I', 0x300).'EPSON');


#for my $i (0x2000..0x4000) {
#    $dvr->PrepareGenericCommand(GPCam::GP_SOCK_TYPE_CMD, GPCam::GPSOCK_MODE_Menu, GPCam::GPSOCK_Menu_CMD_GetParameter, pack('I', $i));
#}

#my $decoded = $dvr->PrepareGenericDownloadCommand(GPCam::PHOTO_GET_REQ, $pkt, "out.dat");

$dvr->disconnect();

__END__

=head1 NAME

./gpcamctl.pl - utility for working with GeneralPlus powered DVR

=head1 SYNOPSIS

./gpcamctl.pl [options]

=head1 OPTIONS

=over 8

=item B<-help>

Print a brief help message and exits.

=item B<-of>

Path to output file filename.

=item B<-u>

Username

=item B<-p>

Password

=item B<-host>

DVR hostname or ip address

=item B<-port>

DVR port

=item B<-c>

DVR command: SetTime, GetParameterFile, GerDeviceStatus, PowerOff, RestartStreaming

=item B<-bt>

Search begin time

=item B<-et>

Search end time

=item B<-dl>

Download found files

=item B<-ch>

Channel number

=item N<-co>

Config option

=item M<-newuserpass>

Password for new user

=item N<-if>

Input file

=item N<-sd>

Set data

=item B<-d>

Debug output

=item B<-fd>

force disconnect after parameter write (Useful when changing IP)

=back

=head1 DESCRIPTION

B<This program> can control GeneralPlus powered DVR

=cut

