# gpcamctl

gpcamctl is an open source cross-platform tool and framework to control GeneralPlus based DVR over
GeneralPlus compatible protocol (libgpcam.so, default control port 8081, vendor app https://play.google.com/store/apps/details?id=generalplus.com.GPCamDemo)

## Usage

gpcamctl.pl -host 192.168.25.1 -port 8081 -c command [-of output_file ]
supported commands:

|command | description |
|--|--|
Download | download video or snapshot specified by --co parameter. Warning! Connect to DVR wifi directly, otherwise you get a lot of errors and broken video on big files
SetTime | set the DVR clock to the urrent system time
GetParameterFile | download parameter xml file contain settings ids, descriptions, default values useful for calling GetParameter and SetParameter commands. If --of is not specified the file contents will be printed to the console
GetDeviceStatus | display the current device status
PowerOff | shutdown the device
RestatStreaming | restart the device media streaming
GetNameList | list recorded files
GetParameter | display value of setting with id specified by --co, the supported setting ids could be found at the parameter file from the GetParameterFile command output 

## Tested hardware

DT190_20200702 (SOC: GPCV5247A, PCB W10_MAIN_V1.0 20200710, WLAN: SV6030P)
https://www.aliexpress.com/item/1005002703912505.html
usb vendor=3322, product=6655
www.generalplus.com, www.southsv.com.cn

## Author and License

Copyright (C) 2021 667bdrm
Licensed under Nestor Makhno Public License



